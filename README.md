# Atividade da Linguagem de Programação Prolog

    - Verifica se um elemento pertence a lista.
    - Verifica se um elemento é o último da lista.
    - Verifica se dois elementos sao consecutivos na lista.
    - Soma de todos os elementos da lista.
    - Posicao do elemento na lista.
    - Número de elementos da lista.
    - Tamanho da lista.
    - Retirar uma ocorrencia de um elemento da lista.
    - Retirar todas as ocorrencias de um elemento da lista.
    - Retirar elementos repetidos da lista.
    - Insere um elemento no início da lista.
    - Insere um elemento em qualquer posiçao da lista.
    - Substituir elemento por outro elemento na lista.
    - Duplica elementos da lista.
    - Permuta elementos da lista.
    - Concatena duas lista.
    - Sublistas de uma lista.
    - Inverter a lista.
    - Maior elemento de uma lista.
    - Dividir uma lista em duas sublistas que contenham os elementos menores e maiores que um dado elemento
    - Verificar se um conjunto está contido no outro
    - Verificar se a interseçao entre dois conjuntos é nao vazia.
    - Verificar se duas listas sao disjuntas (se nao existe algum elementos em comum).
    - Verificar se duas listas sao iguais.
    - Linearizar uma lista
    - Verifica se os elementos de uma lista estao ordenados
    - Ordenar elementos de uma lista em O(n!).
    - Média aritmética de uma lista
    - Produto Vetorial de duas Listas ((x1 * x2) + (x3 * x4).....)
    - Média ponderada de uma lista
    - Insere um elemento no fim de uma lista
    - Remove um elemento da lista
    - Encontra o índice de um elemento em uma lista
    - Encontra o elemento de uma lista a partir do índice
    - Remove um elemento da lista a partir do índice
    - Verifica se um número é par ou ímpar
    - Calcula a mediana de uma Lista
