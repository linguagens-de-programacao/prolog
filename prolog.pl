%LEGENDA:
%   X->Elemento
%   Y->Elemento
%   Z->Elemento
%   C->Cauda
%   H->Cabeça
%   L->Lista

% Verifica se um elemento pertence a lista.
pertence(X, [X|_]).
pertence(X, [_|C]):-pertence(X, C);

% Verifica se um elemento é o último da lista.
ultimo([X], X).
ultimo([_|C], X):-ultimo(C, X).

% Verifica se dois elementos sao consecutivos na lista.
consecutivos(X, Y, [X,Y|_]).
consecutivos(X, Y, [_|C]):-consecutivos(X,Y,C).

% Soma de todos os elementos da lista.
soma([],0).
soma([X|C],S):-soma(C,S1),S is S1+X.

% Posicao do elemento na lista.
n_esimo(1,X,[X|_]).
n_esimo(N,X,[_|C]):-n_esimo(M,X,C),N is M+1.

% Número de elementos da lista.
n_elementos([],0).
n_elementos([X|C],N):-n_elementos(C,N1),N is N1+1.

% Tamanho da lista.
tamanho([_], 1):- !.
tamanho([_|C],N):- tamL(C, N1),N is N1+1.

% Retirar uma ocorrencia de um elemento da lista.
retirar_elemento(X,[X|C],C).
retirar_elemento(X,[X1|C],[X1|C1]):-retirar_elemento(X,C,C1).

% Retirar todas as ocorrencias de um elemento da lista.
retirar_todas(_,[],[]).
retirar_todas(X,[X|C],L):-retirar_todas(X,C,L).
retirar_todas(X,[X1|C],[X1|C1]):-X \== X1, retirar_todas(X,C,C1).

% Retirar elementos repetidos da lista.
retirar_rep([],[]).
retirar_rep([X|C],[X|C1]):-retirar_todas(X,C,L), retirar_rep(L,C1).

% Insere um elemento no início da lista.
inserir_inicio(X, L, [X|L]).

% Insere um elemento em qualquer posiçao da lista.
inserir(X,L,L1):-retirar_elemento(X,L1,L).

% Substituir elemento por outro elemento na lista.
substitui(X,Y,[],[]).
substitui(X,Y,[X|L],[Y|L1]):-substitui(X,Y,L,L1).
substitui(X,Y,[Z|L],[Z|L1]):- X \== Z, substitui(X,Y,L,L1).

% Duplica elementos da lista.
duplica([],[]).
duplica([X|L],[X,X|L1]):-duplica(L,L1).

% Permuta elementos da lista.
permutacao([],[]).
permutacao([X|C],LP):-permutacao(C,C1),inserir(X,C1,LP).

% Concatena duas lista.
concatenar([],L,L).
concatenar([X|L1],L2,[X,L3]):-concatenar(L1,L2,L3).

% Sublistas de uma lista.
sublistas(Sub,L):-concatenar(L1,L2,L), concatenar(Sub,L3,L2).

% Inverter a lista.
inverte([],[]).
inverter([X|C],L):-inverter(C,C1), concatenar(C1,[X],L).

% Maior elemento de uma lista.
maior_elemento([X],Y).
maior_elemento([X,Y|C],Max):-X >= Y, maior_elemento([X,C], Max).
maior_elemento([X,Y|C],Max):-maior_elemento([Y|C],Max).

% Dividir uma lista em duas sublistas que contenham os elementos menores e maiores que um dado elemento
particionar(_,[],[],[]).
particionar(X,[H|L1],[H|LL1],LL2):-H =< X, particionar(X, L1, LL1, LL2).
particionar(X,[H|L1],LL1,[HLL2]):-H > X, particionar(X, L1, LL1, LL2).

% Verificar se um conjunto está contido no outro
esta_contido(L1,L2):-esta_contido(X,L1), esta_contido(X,L2).

% Verificar se a interseçao entre dois conjuntos é nao vazia.
nao_vazia(L1,L2):-pertence(X,L1), pertence(X,L2).

% Verificar se duas listas sao disjuntas (se nao existe algum elementos em comum).
disjuntos(L1,L2):- \+ nao_vazia(L1,L2).

% Verificar se duas listas sao iguais.
listas_iguais([],[]).
listas_iguais([X,L1],L2):-listas_iguais_remover(X,L1,L3), listas_iguais(L1,L3).
listas_iguais_remover(X,[X|Y],Y).
listas_iguais_remover(X,[_|L1],[_|L2]):-listas_iguais_remover(X,L1,L2).


% Linearizar uma lista
linearizar([],[]).
linearizar([H|C], L1):-lista(C), linearizar(H,H1),linearizar(C,C1),concatenar(H1,C1,L1).
linearizar([H|C],[H|C1]):- \+ lista(C),linearizar(C,C1).

%%ORDENAÇÃO
% Verifica se os elementos de uma lista estao ordenados
ordena([X]).
ordena([x,Y|Z]):-maior_elemento(X,Y), ordena([Y|Z]).

% Ordenar elementos de uma lista em O(n!).
ordenar(L,LO):-permutacao(L,LO), ordena(LO), !.


% Média aritmética de uma lista
mediaarit(L,M):- somaLista(L, S), tamL(L,T), M is S / T.

% Produto Vetorial de duas Listas ((x1 * x2) + (x3 * x4).....)
prodVetorial([H], [R], P):- P is H * R, !.
prodVetorial([H|T], [R|S], P):- prodVetorial([H],[R],M), prodVetorial(T,S,U), P is M + U.

% Média ponderada de uma lista
mediapond(L,P,M):- prodVetorial(L,P,PV), somaLista(P,S), M is PV / S.

% Insere um elemento no fim de uma lista
insereFim(T, [H], L):- insereInicio(H,[T],L), !.
insereFim(N, [H|T], L):- insereFim(N,T,X), insereInicio(H, X, L).

% Remove um elemento da lista
remove(X, [X|T], T).
remove(X, [H|T], [H|T1]):- remove(X,T,T1).

% Encontra o índice de um elemento em uma lista
encontraind(E,[E|_],0):- !.
encontraind(E,[_|T],I):- encontra(E,T,X), I is X + 1.

% Encontra o elemento de uma lista a partir do índice
encontraeElem(0, [H|_], H):- !.
encontraeElem(I, [_|T], E):- X is I - 1, encontraeElem(X, T, E).

% Remove um elemento da lista a partir do índice
removeind(0,[_|T],T):- !.
removeind(I,[H|T],R):- X is I - 1, removeind(X, T, Y), insereInicio(H, Y, R).

% Verifica se um número é par ou ímpar
par(N):- N mod 2 =:= 0.
impar(N):- N mod 2 =:= 1.

% Calcula a mediana de uma Lista
mediana(L, M):- tamL(L, T), par(T), X is (T / 2) - 1, encontraeElem(X, L, E1), X2 is X + 1, encontraeElem(X2, L, E2), M is (E1 + E2) / 2,!.
mediana(L, M):- tamL(L, T), impar(T), X is (T - 1) / 2, encontraeElem(X, L, M),!.
